// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained 
// from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and 
// log its length.

const problem5 = (inventory,years) => {
    if(inventory==undefined){
        return "please provide years array";
    }
    else if(inventory.length == 0 || years == undefined){
        return [];
    }
    let olderCars = [];
    let oldYears = [];
    for(let i =0;i<years.length;i++){
        if(years[i]<2000){
            oldYears.push(years[i]);
        }
        if(oldYears.includes(inventory[i].car_year)){
            olderCars.push(inventory[i]);
        }

    }

    return olderCars;
}

module.exports = problem5;


