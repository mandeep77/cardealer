// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. 
//Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it 
//was returned.

const problem3 = inventory => {
    if(inventory==undefined){
        return "please provide inventory array";
    }
    else if(inventory.length == 0){
        return [];
    }

    inventory.sort(function(a,b){
        if(a.car_model>b.car_model){
            return 1;
        } else {
            return -1;
        }
    });
    let models = [];
    for(let i =0;i<inventory.length;i++){
        models.push(inventory[i].car_model);
    }
    return models;
}

module.exports = problem3;
