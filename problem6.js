// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory. 
// Execute a function and return an array that only contains BMW and Audi cars.  
// Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const Problem6 = (inventory,car1,car2) => {
    if(inventory==undefined){
        return "please provide inventory array";
    }
    else if(inventory.length == 0 || car1 == undefined && car2 ==undefined){
        return [];
    }
    let BMWAndAudi = [];
    for(let i =0; i<inventory.length;i++){
        if(inventory[i].car_make === car1 || inventory[i].car_make === car2) {
            BMWAndAudi.push(inventory[i]);
        }

    }
    const fliterd = JSON.stringify(BMWAndAudi);
    return fliterd;
}

module.exports = Problem6;
