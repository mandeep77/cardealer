// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"
const problem2 = inventory => {
    if(inventory==undefined){
        return "please provide inventory array";
    }
    else if(inventory.length == 0){
        return [];
    }
    for(let i =0;i<inventory.length;i++){
        let length = inventory.length;
        if(inventory[i].id ===length){
            return (`Last car is a ${inventory[i].car_make} ${inventory[i].car_model}`);
        }
    }
}
module.exports = problem2;