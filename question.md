***Q1. What is the role of script in package.json???***

- A package.json is a JSON file that exists in the roots of a JavaScript Project, it holds metadata relevant to the project and is used to manage the project's dependencies, scripts, versions alot more.

**Metadata** - a set of data that describes and gives information about other data.

***Q2. Is there any difference b/w dependencies & dev-dependencies ??? List down all the differences***

- yes, Dependencies and dev-Dependencies are different -
   
   - **Dependencies**- Is used in production.
   - **Dev-Dependencies**- is used in developing and testing.

- Dev-Dependencies modules are only required during developing, while Dependencies modules are required during runtime.  

***Q3. Find out what a circular reference inside an object is?***

- Object that refers to itself.
Example:
```
function hello() {
  this.abc = "Hello world";
  this.circular = this;
}

var hello = new hello();

```
***Q4. find out what pass by reference and pass by value in javascript?***

- In JS the primitive data type like string, number,boolean etc are passed by value.
- and objects array and function are pass by reference.
- Primitive data types are passed, or copied, by value and are immutable, meaning that the existing value cannot be altered the way an array or an object can.
- Objects, on the other hand, are passed by reference and point to a location in memory for the value, not the value itself. 

***Javascript always passes by value, but in an array or object, the value is a reference to it, so you can 'change' the contents.***

***Q4. Find out what the prototype in javascript is?***

All JavaScript objects inherit properties and methods from a prototype:

   - Date objects inherit from Date.prototype
   - Array objects inherit from Array.prototype
   - Person objects inherit from Person.prototype

The **Object.prototype** is on the top of the prototype inheritance chain:

Date objects, Array objects, and Person objects inherit from Object.prototype.

***The JavaScript prototype property allows you to add new properties to object constructors:***

***Q5. Find out the difference between a regular function and an arrow function?***

Syntax of regular function:
```

let x = function function_name(parameters){ 
   // body of the function 
}; 
```
Syntax of arrow function:
```
let x = parameters =>{ 
   // body of the function 
}; 
```
**While both regular and arrow functions work in a similar manner, yet there are certain interesting differences between them:**

- **arrow functions** do not have their own this.
- Arguments objects are **not** available in **arrow functions**, but are **available** in **regular functions.** 
- Regular functions created using function declarations or expressions are ‘constructible’ and ‘callable’. Since regular functions are constructible, they can be called using the ‘new’ keyword. However, the arrow functions are only ‘callable’ and not constructible.


























